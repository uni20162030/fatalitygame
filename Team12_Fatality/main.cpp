/*
 * Skeleton code for CSE471 Fall 2019
 *
 * Won-Ki Jeong, wkjeong@unist.ac.kr
 */

#include <stdio.h>
#include<vector>
#include<algorithm>
#include<unordered_map>
#include<time.h>
#include<assert.h>
#include <GL/glew.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <iostream>
#include <assert.h>
#include<math.h>

#include <GL/freeglut.h>
#include <stdlib.h>
#include <stdio.h>

 //
 // Definitions
 //

#define Pi 3.14159265

using namespace std;

typedef struct {
	unsigned char x, y, z, w;
} uchar4;
typedef unsigned char uchar;

// BMP loader
void LoadBMPFile(uchar4** dst, int* width, int* height, const char* name);

#define screenSize 1024
#define	imageSize 512

//
// Variables
//

bool zm = 0, rot = 0, act = 0;
int lastX = 0, lastY = 0;
float				sh = 1.0, thx = 0, thy = 0.6, thz = 0,
					sb = 1.0, tbx = 0, tby = 0, tbz = 0,
					slcl = 1.0, tlclx = -0.12, tlcly = -0.54, tlclz = -0.24,
					slcr = 1.0, tlcrx = 0.12, tlcry = -0.54, tlcrz = -0.24,
					slfl = 1.0, tlflx = -0.12, tlfly = -0.54, tlflz = 0.24,
					slfr = 1.0, tlfrx = 0.12, tlfry = -0.54, tlfrz = 0.24;
GLfloat rotate_x = 0.0, rotate_y = 0.0, zoom = 1.0;
float pos[3] = { 0,0,0.1 };
float up[3] = { 0,1,0 };
float rt[3] = { 1,0,0 };
float temp1, temp2;
vector<string> imgs = {"..\\unist.bmp", "..\\yuni.bmp", "..\\dance.bmp", "..\\A+.bmp", "..\\end.bmp"};
auto k = imgs.begin();

// array to store synthetic cubemap raw image
static GLubyte image1[4][4][4];
static GLubyte image2[4][4][4];
static GLubyte image3[4][4][4];
static GLubyte image4[4][4][4];
static GLubyte image5[4][4][4];
static GLubyte image6[4][4][4];

// texture object handles, FBO handles
GLuint color_tex[1];
GLuint fb, depth_rb;

//
// Functions
//


//////////////////////////////////MENU/////////////////////////////////////////////////////////////////////////////////////////////

bool ext = 1, menu = 1, win = 0;
int level;

struct Mouse
{
	int x;		
	int y;		
	int lmb;	
	int mmb;	
	int rmb;	
	int xpress; 
	int ypress; 
};

typedef struct Mouse Mouse;

Mouse TheMouse = { 0,0,0,0,0 };

int winw = 640;
int winh = 480;

typedef void (*ButtonCallback)();

struct Button
{
	int   x;							
	int   y;							
	int   w;							
	int   h;							
	int	  state;						
	int	  highlighted;					
	char* label;						
	ButtonCallback callbackFunction;	

	int id;								

	struct Button* next;				
};
typedef struct Button Button;


Button* pButtonList = NULL;

int GlobalRef = 0;

int CreateButton(char* label, ButtonCallback cb, int x, int y, int w, int h)
{
	Button* p = (Button*)malloc(sizeof(Button));
	assert(p);
	memset(p, 0, sizeof(Button));
	p->x = x;
	p->y = y;
	p->w = w;
	p->h = h;
	p->callbackFunction = cb;
	p->label = (char*)malloc(strlen(label) + 1);
	if (p->label)
		sprintf(p->label, label);

	p->next = pButtonList;
	pButtonList = p;

	return p->id = ++GlobalRef;
}


static void Easy() {
	level = 1;
	menu = 0;
}

static void Medium() {
	level = 3;
	menu = 0;
}

static void Hard() {
	level = 6;
	menu = 0;
}

static void Exit() {
	ext = 0;
}

void Font(void* font, char* text, int x, int y)
{
	glRasterPos2i(x, y);

	while (*text != '\0')
	{
		glutBitmapCharacter(font, *text);
		++text;
	}
}

int ButtonClickTest(Button* b, int x, int y)
{
	if (b)
	{
		if (x > b->x&&
			x < b->x + b->w &&
			y > b->y&&
			y < b->y + b->h) {
			return 1;
		}
	}
	return 0;
}

void ButtonRelease(int x, int y)
{
	Button* b = pButtonList;
	while (b)
	{
		if (ButtonClickTest(b, TheMouse.xpress, TheMouse.ypress) &&
			ButtonClickTest(b, x, y))
		{
			if (b->callbackFunction) {
				b->callbackFunction();
			}
		}

		b->state = 0;

		b = b->next;
	}
}

void ButtonPress(int x, int y)
{
	Button* b = pButtonList;
	while (b)
	{
		if (ButtonClickTest(b, x, y))
		{
			b->state = 1;
		}
		b = b->next;
	}
}

void ButtonPassive(int x, int y)
{
	int needRedraw = 0;
	Button* b = pButtonList;
	while (b)
	{
		if (ButtonClickTest(b, x, y))
		{
			if (b->highlighted == 0) {
				b->highlighted = 1;
				needRedraw = 1;
			}
		}
		else if (b->highlighted == 1)
			{
				b->highlighted = 0;
				needRedraw = 1;
			}
		b = b->next;
	}
	if (needRedraw) {
		glutPostRedisplay();
	}
}

void ButtonDraw()
{
	int fontx;
	int fonty;

	Button* b = pButtonList;
	while (b)
	{
		if (b->highlighted)
			glColor3f(0.7f, 0.7f, 0.8f);
		else
			glColor3f(0.6f, 0.6f, 0.6f);

		glBegin(GL_QUADS);
		glVertex2i(b->x, b->y);
		glVertex2i(b->x, b->y + b->h);
		glVertex2i(b->x + b->w, b->y + b->h);
		glVertex2i(b->x + b->w, b->y);
		glEnd();

		glLineWidth(3);

		if (b->state)
			glColor3f(0.4f, 0.4f, 0.4f);
		else
			glColor3f(0.8f, 0.8f, 0.8f);

		glBegin(GL_LINE_STRIP);
		glVertex2i(b->x + b->w, b->y);
		glVertex2i(b->x, b->y);
		glVertex2i(b->x, b->y + b->h);
		glEnd();

		if (b->state)
			glColor3f(0.8f, 0.8f, 0.8f);
		else
			glColor3f(0.4f, 0.4f, 0.4f);

		glBegin(GL_LINE_STRIP);
		glVertex2i(b->x, b->y + b->h);
		glVertex2i(b->x + b->w, b->y + b->h);
		glVertex2i(b->x + b->w, b->y);
		glEnd();

		glLineWidth(1);


		fontx = b->x + (b->w - glutBitmapLength(GLUT_BITMAP_HELVETICA_10, (unsigned char*)(b->label))) / 2;
		fonty = b->y + (b->h + 10) / 2;

		if (b->state) {
			fontx += 2;
			fonty += 2;
		}

		if (b->highlighted)
		{
			glColor3f(0, 0, 0);
			Font(GLUT_BITMAP_HELVETICA_10, b->label, fontx, fonty);
			fontx--;
			fonty--;
		}

		glColor3f(1, 1, 1);
		Font(GLUT_BITMAP_HELVETICA_10, b->label, fontx, fonty);

		b = b->next;
	}
}



void MenuInit()
{
	glEnable(GL_LIGHT0);

	CreateButton("Easy", Easy, 255, 140, 100, 30);
	CreateButton("Medium", Medium, 255, 190, 100, 30);
	CreateButton("Hard", Hard, 255, 240, 100, 30);
	CreateButton("Exit", Exit, 255, 290, 100, 30);
}


void Draw2D()
{
	ButtonDraw();
}

void MenuDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT |
		GL_DEPTH_BUFFER_BIT);

	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, winw, winh, 0, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Draw2D();

	glutSwapBuffers();
	if (!menu || !ext) {
		glutLeaveMainLoop();
	}
}

void MenuResize(int w, int h)
{
	winw = w;
	winh = h;

	glViewport(0, 0, w, h);
}


void MenuMouseButton(int button, int state, int x, int y)
{
	TheMouse.x = x;
	TheMouse.y = y;

	if (state == GLUT_DOWN)
	{
		TheMouse.xpress = x;
		TheMouse.ypress = y;

		switch (button)
		{
		case GLUT_LEFT_BUTTON:
			TheMouse.lmb = 1;
			ButtonPress(x, y);
		case GLUT_MIDDLE_BUTTON:
			TheMouse.mmb = 1;
			break;
		case GLUT_RIGHT_BUTTON:
			TheMouse.rmb = 1;
			break;
		}
	}
	else
	{
		switch (button)
		{
		case GLUT_LEFT_BUTTON:
			TheMouse.lmb = 0;
			ButtonRelease(x, y);
			break;
		case GLUT_MIDDLE_BUTTON:
			TheMouse.mmb = 0;
			break;
		case GLUT_RIGHT_BUTTON:
			TheMouse.rmb = 0;
			break;
		}
	}

	glutPostRedisplay();
}


void MenuMouseMotion(int x, int y)
{
	int dx = x - TheMouse.x;
	int dy = y - TheMouse.y;

	TheMouse.x = x;
	TheMouse.y = y;

	ButtonPassive(x, y);

	glutPostRedisplay();
}


void MenuMousePassiveMotion(int x, int y)
{
	
	int dx = x - TheMouse.x;
	int dy = y - TheMouse.y;

	TheMouse.x = x;
	TheMouse.y = y;

	ButtonPassive(x, y);
}

//////////////////////////////////END_MENU/////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////CUTTING/////////////////////////////////////////////////////////////////////////////////////////

#define F first
#define S second
#define pdd pair < double , double >

double eps = 1e-6;

vector < vector < pdd > > triangles;
vector <double> depth;

double getRandom01() {
	double r = rand() % 100;
	r /= 100;
	return r;
}

double cornersX[] = { 0,   0, 1.0, 1.0, 0 };
double cornersY[] = { 0, 1.0, 1.0,   0, 0 };

pdd getRandomPointOnSegment(pdd s, pdd v) {
	double r = getRandom01();
	r *= 0.4;
	r += 0.3;
	return { s.F + v.F * r, s.S + v.S * r };
}

vector<pdd> triangleArray(pdd a, pdd b, pdd c) {
	vector < pdd > res;
	res.push_back(a);
	res.push_back(b);
	res.push_back(c);
	return res;
}

void addMoreTriangles(pdd a, pdd b, pdd c, int lvl) {
	/*
			b
		   /|
		  / |
		 a  |
		  \ p
		   \|
			c
	*/
	if (lvl == 0)
		return;
	pdd bc = { c.F - b.F, c.S - b.S }; // vector B to C
	pdd p = getRandomPointOnSegment(b, bc);
	triangles.pop_back();
	triangles.push_back(triangleArray(p, a, c));
	addMoreTriangles(p, a, c, lvl - 1);
	triangles.push_back(triangleArray(p, a, b));
	addMoreTriangles(p, a, b, lvl - 1);
}

void cutRandomly(int difficulty) {
	double x = getRandom01(), y = getRandom01();
	for (int i = 1;i < 5;++i) {
		triangles.push_back(triangleArray(
			{ x, y },
			{ cornersX[i - 1], cornersY[i - 1] },
			{ cornersX[i], cornersY[i] } 
		));
		addMoreTriangles({ x, y }, { cornersX[i - 1], cornersY[i - 1] },
			{ cornersX[i], cornersY[i] }, difficulty);
	}
}


////////////////////////////////END_CUTTING////////////////////////////////////////////////////////////////////////////////////

// create synthetic data for static cubemap
void makeSyntheticImages(void)
{
	int i, j, c;

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			c = ((((i & 0x1) == 0) ^ ((j & 0x1)) == 0)) * 255;
			image1[i][j][0] = (GLubyte)c;
			image1[i][j][1] = (GLubyte)c;
			image1[i][j][2] = (GLubyte)c;
			image1[i][j][3] = (GLubyte)255;

			image2[i][j][0] = (GLubyte)c;
			image2[i][j][1] = (GLubyte)c;
			image2[i][j][2] = (GLubyte)0;
			image2[i][j][3] = (GLubyte)255;

			image3[i][j][0] = (GLubyte)c;
			image3[i][j][1] = (GLubyte)0;
			image3[i][j][2] = (GLubyte)c;
			image3[i][j][3] = (GLubyte)255;

			image4[i][j][0] = (GLubyte)0;
			image4[i][j][1] = (GLubyte)c;
			image4[i][j][2] = (GLubyte)c;
			image4[i][j][3] = (GLubyte)255;

			image5[i][j][0] = (GLubyte)255;
			image5[i][j][1] = (GLubyte)c;
			image5[i][j][2] = (GLubyte)c;
			image5[i][j][3] = (GLubyte)255;

			image6[i][j][0] = (GLubyte)c;
			image6[i][j][1] = (GLubyte)c;
			image6[i][j][2] = (GLubyte)255;
			image6[i][j][3] = (GLubyte)255;
		}
	}
}

double ratio;

float* normalize(float* a) {
	float* t = new float[3];
	t[0] = a[0] / sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
	t[1] = a[1] / sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
	t[2] = a[2] / sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
	return t;
}
float* cross_product(float* a, float* b) {
	float* t = new float[3];
	t[0] = a[1] * b[2] - b[1] * a[2];
	t[1] = a[2] * b[0] - b[2] * a[0];
	t[2] = a[0] * b[1] - b[0] * a[1];
	return t;
}

void GameMouseMotion(int x, int y) {
	float* t1 = new float[3]{ -pos[0], -pos[1], -pos[2] };
	float* t2 = normalize(cross_product(up, t1));
	float t3[3];
	t3[0] = pos[0] + t2[0] * (x - lastX) / 100000;
	t3[1] = pos[1] + t2[1] * (x - lastX) / 100000;
	t3[2] = pos[2] + t2[2] * (x - lastX) / 100000;
	t2 = normalize(t3);
	pos[0] = t2[0] * 0.1;
	pos[1] = t2[1] * 0.1;
	pos[2] = t2[2] * 0.1;

	t1 = new float[3]{ -pos[0], -pos[1], -pos[2] };
	t2 = normalize(cross_product(up, t1));
	t3[0] = pos[0] + up[0] * (y - lastY) / 100000;
	t3[1] = pos[1] + up[1] * (y - lastY) / 100000;
	t3[2] = pos[2] + up[2] * (y - lastY) / 100000;
	t2 = normalize(cross_product(t1, t2));
	up[0] = t2[0];
	up[1] = t2[1];
	up[2] = t2[2];

	t2 = normalize(t3);
	pos[0] = t2[0] * 0.1;
	pos[1] = t2[1] * 0.1;
	pos[2] = t2[2] * 0.1;
}

void GameInit(int level)
{
	// You need to ues glew
	glewInit();
	triangles.clear();
	cutRandomly(level);
	for (auto i : triangles) {
		double x = getRandom01(), y = getRandom01(), z = getRandom01();
		x -= 0.5;
		y -= 0.5;
		z -= 0.5;
		depth.push_back(x);
		depth.push_back(y);
		depth.push_back(z);
	}

	GLfloat diffuse[4] = { 1.0, 1.0, 1.0, 1.0 };

	glClearColor(1.0, 1.0, 1.0, 1.0);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);

	//
	// Creating a 2D texture from image
	//
	uchar4* dst;
	int width, height;
	LoadBMPFile(&dst, &width, &height, (*k).c_str()); // this is how to load image
	if (k == --imgs.end()) k = imgs.begin();
	else k++;
	ratio = height;
	ratio /= width;
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, color_tex);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, color_tex[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, dst);
	int startx = int(getRandom01() * 4000.0) + 4000;
	int starty = int(getRandom01() * 4000.0) + 4000;
	lastX = 0;
	lastY = 0;
	up[0] = 0.0; up[1] = 1.0; up[2] = 0.0;
	pos[0] = 0.0; pos[1] = 0.0; pos[2] = 0.1;
	GameMouseMotion(startx, starty);
}

void GameMouseButton(int button, int state, int xpos, int ypos)
{
	if (state == 0) {
		lastX = xpos;
		lastY = ypos;
		if (button == 2) zm = 1;
		else if (button == 0) rot = 1;
		act = 1;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
	float sensitivity = 0.001f; // change this value to your liking

	xoffset *= sensitivity;
	yoffset *= sensitivity;

	if (button == 0 && state == 1) {

		rot = 0;
	}


	if (state == 1 && button == 2) {
		zm = 0;
	}
	if (state = 1) act = 0;
	//	cameraFront = normalize(front);
}

void GameIdle()
{
	// do something for animation here b/c this will be called when idling

	glutPostRedisplay();
}


void GameDisplay(void)
{
	if (menu) {
		pos[0] = 0.0; pos[1] = 0.0; 
		if (pos[2] > 0) pos[2] = 0.1;
		else pos[2] = -0.1;
		//up[0] = 0.0; up[1] = 1.0; up[2] = 0.0;
	}
	//  Clear screen and Z-buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, color_tex[0]);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glLoadIdentity();
	gluLookAt(pos[0], pos[1], pos[2], 0, 0, 0, up[0], up[1], up[2]);
	
	glBegin(GL_TRIANGLES);
	int loc = 0;
	for (auto i : triangles) {
		glTexCoord2f(i[0].first, i[0].second); glVertex3f(i[0].first - 0.5, ratio*(i[0].second - 0.5), depth[loc]);
		glTexCoord2f(i[1].first, i[1].second); glVertex3f(i[1].first - 0.5, ratio*(i[1].second - 0.5), depth[loc+1]);
		glTexCoord2f(i[2].first, i[2].second); glVertex3f(i[2].first - 0.5, ratio*(i[2].second - 0.5), depth[loc+2]);
		loc += 3;
	}
	glEnd();
	if (menu) {
		glLoadIdentity();
		glBindTexture(GL_TEXTURE_2D, color_tex[0]);
		glBegin(GL_QUADS);
		up[0] = 0.0; up[1] = 1.0; up[2] = 0.0;
		pos[0] = 0.0; pos[1] = 0.0; pos[2] = 0.1;
		gluLookAt(pos[0], pos[1], pos[2], 0, 0, 0, up[0], up[1], up[2]);
		glTexCoord2f(0, 0); glVertex3f(0.6, 1 - 0.4*ratio, 0.0);
		glTexCoord2f(0, 1); glVertex3f(0.6 , 1, 0.0);
		glTexCoord2f(1, 1); glVertex3f(1.0 , 1, 0.0);
		glTexCoord2f(1, 0); glVertex3f(1.0, 1 - 0.4*ratio, 0.0);
		glEnd();
		glFlush();
		glDisable(GL_TEXTURE_2D);
		glutSwapBuffers();
		Sleep(3000);
		glutLeaveMainLoop();
	}
	else {
		glFlush();
		glDisable(GL_TEXTURE_2D);
		glutSwapBuffers();
	}
	int X = int(rotate_x), Y = int(rotate_y);
	if (abs(pos[2]) > 0.09999) menu = 1;
}

void GameResize(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y)
{

}

int main(int argc, char** argv)
{
	srand(time(NULL));
	random_shuffle(imgs.begin(), --imgs.end());
	while (ext) {
		if (menu) {
			glutInit(&argc, argv);
			glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
			glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
			glutInitWindowSize(winw, winh);
			glutInitWindowPosition(200, 100);
			glutCreateWindow("ROTALITY");
			glutDisplayFunc(MenuDisplay);
			glutReshapeFunc(MenuResize);
			glutMouseFunc(MenuMouseButton);
			glutMotionFunc(MenuMouseMotion);
			glutPassiveMotionFunc(MenuMousePassiveMotion);

			MenuInit();

			glutMainLoop();
		}
		else {
			glutInit(&argc, argv);
			glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
			glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
			glutInitWindowSize(screenSize, screenSize);
			glutInitWindowPosition(100, 100);
			glutCreateWindow("GAME");
			GameInit(level);
			glutDisplayFunc(GameDisplay);
			glutIdleFunc(GameIdle);
			glutReshapeFunc(GameResize);
			glutMouseFunc(GameMouseButton);
			glutMotionFunc(GameMouseMotion);
			glutKeyboardFunc(keyboard);
			glutMainLoop();
			menu = 1;
		}
	}
	return 0;
}
